
# blockly_executor_core

В репозитории находятся базовые блоки и алгоритмы, необходимые для выполнения автоматических операций

## Дополнительная информация[:](https://online.sbis.ru/opendoc.html?guid=dc05a526-5c7b-439d-8860-a5c77a76c3bf)

- Ответственный: [Разговоров М. А.](https://online.sbis.ru/person/a6ccda3a-a3a5-4bcc-a76d-232bf3d305c8)

- [Техническая документация](https://online.sbis.ru/shared/disk/f3db3864-433e-47cd-b7bc-86efa801cb97)
